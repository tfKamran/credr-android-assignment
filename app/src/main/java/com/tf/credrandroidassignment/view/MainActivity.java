package com.tf.credrandroidassignment.view;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tf.credrandroidassignment.R;
import com.tf.credrandroidassignment.model.SearchResult;
import com.tf.credrandroidassignment.network.SearchHelper;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements TextWatcher, SearchHelper.SearchResultsFetchedListener {

    private SearchHelper mSearchHelper;
    private SearchResultAdapter mAdapter;
    private View mProgressBar;
    private TextView lblMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSearchHelper = new SearchHelper(MainActivity.this);
        mAdapter = new SearchResultAdapter();

        lblMessage = (TextView) findViewById(R.id.lbl_message);
        mProgressBar = findViewById(R.id.progress_bar);
        EditText txtSearch = (EditText) findViewById(R.id.txt_search);
        RecyclerView mListSearchResults = (RecyclerView) findViewById(R.id.list_results);

        mListSearchResults.setLayoutManager(new GridLayoutManager(MainActivity.this, getMaxColumns()));
        mListSearchResults.setAdapter(mAdapter);

        txtSearch.addTextChangedListener(this);

        showMessage(R.string.search_results_hint);
    }

    private int getMaxColumns() {
        return (int) Math.floor(getResources().getDisplayMetrics().widthPixels
                / getResources().getDimension(R.dimen.search_result_total_width));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable newQuery) {
        if (newQuery.length() > 0) {
            mSearchHelper.fetchSearchResults(newQuery.toString(), this);

            mProgressBar.setVisibility(View.VISIBLE);
            hideMessage();
        } else {
            mSearchHelper.cancelAllRequests();
            mAdapter.clearResults();

            mProgressBar.setVisibility(View.GONE);
            showMessage(R.string.search_results_hint);
        }
    }

    @Override
    public void onFetched(List<SearchResult> results) {
        if (results.size() == 0) {
            showMessage(R.string.search_results_empty);
        } else {
            hideMessage();
        }

        mAdapter.updateResults(results);
        mProgressBar.setVisibility(View.GONE);
    }

    private void showMessage(int string) {
        lblMessage.setText(string);
        lblMessage.setVisibility(View.VISIBLE);
    }

    private void hideMessage() {
        lblMessage.setVisibility(View.GONE);
    }

    private class SearchResultAdapter extends RecyclerView.Adapter<SearchResultViewHolder> {

        List<SearchResult> mItems = new ArrayList<>();

        @Override
        public SearchResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new SearchResultViewHolder(LayoutInflater.from(MainActivity.this)
                    .inflate(R.layout.search_result_item, parent, false));
        }

        @Override
        public void onBindViewHolder(SearchResultViewHolder holder, int position) {
            final SearchResult searchResult = mItems.get(position);

            Glide.with(MainActivity.this)
                    .load(Uri.parse(searchResult.getThumbnailUrl()))
                    .into(holder.imgThumbnail);

            holder.lblTitle.setText(searchResult.getItemName());
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        void updateResults(List<SearchResult> items) {
            mItems.clear();
            mItems.addAll(items);

            notifyDataSetChanged();
        }

        void clearResults() {
            mItems.clear();

            notifyDataSetChanged();
        }
    }

    private class SearchResultViewHolder extends RecyclerView.ViewHolder {

        ImageView imgThumbnail;
        TextView lblTitle;

        SearchResultViewHolder(View itemView) {
            super(itemView);

            imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
            lblTitle = (TextView) itemView.findViewById(R.id.lbl_title);
        }
    }
}
