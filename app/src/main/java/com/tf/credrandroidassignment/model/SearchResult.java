package com.tf.credrandroidassignment.model;

/**
 * Created by kamran on 28/10/17.
 */

public class SearchResult {

    private String thumbnailUrl;
    private String itemName;

    public SearchResult(String thumbnailUrl, String itemName) {
        this.thumbnailUrl = thumbnailUrl;
        this.itemName = itemName;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
