package com.tf.credrandroidassignment.network;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;

import com.tf.credrandroidassignment.model.SearchResult;
import com.tf.credrandroidassignment.util.WebPageParser;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamran on 28/10/17.
 */

public class SearchHelper {

    private final Context mContext;
    private AsyncTask mAsyncTask = null;

    public SearchHelper(Context context) {
        mContext = context;
    }

    public AsyncTask fetchSearchResults(final String query, final SearchResultsFetchedListener listener) {
        cancelAllRequests();

        return mAsyncTask = new AsyncTask<Void, Void, List<SearchResult>>() {
            @Override
            protected List<SearchResult> doInBackground(Void... params) {
                final ArrayList<SearchResult> searchResults = new ArrayList<>();

                try {
                    final Network.NetworkResponse networkResponse = fetchSearchPage(query);

                    WebPageParser webPageParser = new WebPageParser();
                    if (networkResponse != null && networkResponse.getResponseCode() == 200) {
                        searchResults.addAll(webPageParser.parse(networkResponse.getResponseString()));
                    }
                } catch (Network.NoInternetException | IOException | JSONException e) {
                    e.printStackTrace();
                }

                return searchResults;
            }

            @Override
            protected void onPostExecute(List<SearchResult> searchResults) {
                super.onPostExecute(searchResults);

                if (listener != null) {
                    listener.onFetched(searchResults);
                }
            }
        }.execute();
    }

    public Network.NetworkResponse fetchSearchPage(String query) throws Network.NoInternetException, IOException, JSONException {
        ContentValues contentValues = new ContentValues();
        contentValues.put("keyword", query);
        contentValues.put("sort", "rlvncy");

        return Network.requestGet(mContext, Network.BASE_URL, contentValues);
    }

    public void cancelAllRequests() {
        if (mAsyncTask != null && !mAsyncTask.isCancelled()) {
            mAsyncTask.cancel(true);
        }
    }

    public interface SearchResultsFetchedListener {
        void onFetched(List<SearchResult> results);
    }
}
