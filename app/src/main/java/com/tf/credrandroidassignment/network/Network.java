package com.tf.credrandroidassignment.network;

import android.content.ContentValues;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Set;

/**
 * Created by kamran on 28/10/17.
 */

public class Network {

    private static final String TAG = "Network";
    private static final int TIMEOUT = 60000;
    private static final String GET = "GET";
    private static final String POST = "POST";
    private static final String PUT = "PUT";

    public static final String BASE_URL = "https://www.snapdeal.com/search";

    public static NetworkResponse requestGet(final Context context, final String urlString, final ContentValues contentValues) throws NoInternetException, IOException, JSONException {
        return request(context, urlString, contentValues, GET);
    }

    public static NetworkResponse requestPost(Context context, String urlString, ContentValues contentValues) throws NoInternetException, IOException, JSONException {
        return request(context, urlString, contentValues, POST);
    }

    public static NetworkResponse requestPost(Context context, String urlString, JSONObject contentValues) throws NoInternetException, IOException, JSONException {
        return request(context, urlString, contentValues, POST);
    }

    public static NetworkResponse requestPut(Context context, String urlString, ContentValues contentValues) throws NoInternetException, IOException, JSONException {
        return request(context, urlString, contentValues, PUT);
    }

    public static InputStream requestStream(Context context, String urlString) throws IOException, NoInternetException {
        if (isConnectedToInternet(context)) {
            InputStream inputStream = null;

            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(TIMEOUT);
            conn.setConnectTimeout(TIMEOUT);
            conn.setRequestMethod(GET);
            conn.setDoInput(true);

            conn.connect();
            int responseCode = conn.getResponseCode();
            if (responseCode < 400) {
                inputStream = conn.getInputStream();
            }

            Log.i(TAG, urlString);
            Log.i(TAG, "HTTP Response Code: " + responseCode);

            return inputStream;
        } else {
            throw new NoInternetException();
        }
    }

    private static NetworkResponse request(final Context context, String urlString, final Object requestParameters, final String method) throws NoInternetException, IOException, JSONException {
        if (isConnectedToInternet(context)) {
            InputStream inputStream = null;

            NetworkResponse responseString = new NetworkResponse(0, "");

            try {
                if (method.equals(GET)) {
                    urlString = urlString + getQueryParameters(urlString, ((ContentValues) requestParameters));
                }

                URL url = new URL(urlString);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(TIMEOUT);
                conn.setConnectTimeout(TIMEOUT);
                conn.setRequestMethod(method);
                conn.setDoInput(true);
                conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");

                if (method.equals(POST)) {
                    conn.setDoOutput(true);

                    if (requestParameters instanceof ContentValues) {
                        addRequestParameters(((ContentValues) requestParameters), conn);
                    } else if (requestParameters instanceof JSONObject) {
                        addRequestParameters(((JSONObject) requestParameters), conn);
                    }
                } else {
                    conn.setDoOutput(false);
                }

                conn.connect();
                int responseCode = conn.getResponseCode();
                if (responseCode < 400) {
                    inputStream = conn.getInputStream();
                } else {
                    inputStream = conn.getErrorStream();
                }

                String contentAsString = inputStreamToString(inputStream);

                Log.i(TAG, urlString);
                Log.i(TAG, requestParameters.toString());
                Log.i(TAG, "HTTP Response Code: " + responseCode);
//                Log.i(TAG, contentAsString);

                responseString = new NetworkResponse(responseCode, contentAsString);
            } finally {
                try {
                    if (inputStream != null)
                        inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return responseString;
        } else {
            throw new NoInternetException();
        }
    }

    private static void addRequestParameters(ContentValues contentValues, HttpURLConnection conn) throws IOException, JSONException {
        conn.setRequestProperty("Content-Type", "application/json");

        String output = getJSONObject(contentValues).toString();

        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        writer.write(output);
        writer.flush();
        writer.close();
    }

    private static void addRequestParameters(JSONObject jsonObject, HttpURLConnection conn) throws IOException, JSONException {
        conn.setRequestProperty("Content-Type", "application/json");

        String output = jsonObject.toString();

        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        writer.write(output);
        writer.flush();
        writer.close();
    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private static JSONObject getJSONObject(ContentValues contentValues) throws JSONException {
        JSONObject jsonObject = new JSONObject();

        Set<String> keySet = contentValues.keySet();
        for (String key : keySet) {
            jsonObject.put(key, contentValues.get(key));
        }

        return jsonObject;
    }

    private static String getQueryParameters(String url,ContentValues contentValues) {
        String parameters = "";

        Set<String> keySet = contentValues.keySet();
        for (String key : keySet) {
            try {
                parameters += "&" + key + "=" + URLEncoder.encode(String.valueOf(contentValues.get(key)), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        return parameters.length() > 0 ? "?" + parameters.substring(1) : parameters;
    }

    public static String inputStreamToString(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder stringBuilder = new StringBuilder();

        try {
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return stringBuilder.toString().trim();
    }

    public static class NoInternetException extends Exception {
    }

    public static class NetworkResponse {
        private final int responseCode;
        private final String responseString;

        public NetworkResponse(int responseCode, String responseString) {
            this.responseCode = responseCode;
            this.responseString = responseString;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public String getResponseString() {
            return responseString;
        }

        @Override
        public String toString() {
            return getResponseString();
        }
    }

}
