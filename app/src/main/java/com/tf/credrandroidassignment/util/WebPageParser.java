package com.tf.credrandroidassignment.util;

import com.tf.credrandroidassignment.model.SearchResult;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamran on 28/10/17.
 */

public class WebPageParser {

    public List<SearchResult> parse(String page) {
        List<SearchResult> results = new ArrayList<>();

        final Document document = Jsoup.parse(page);
        final Elements products = document.select("div.product-tuple-listing");

        for (int index = 0; index < products.size(); index++) {
            final Element product = products.get(index);

            results.add(new SearchResult(product.select("source").attr("srcset"),
                    product.select("p.product-title ").html()));
        }

        return results;
    }
}
