package com.tf.credrandroidassignment.network;

import android.content.Context;
import android.support.v4.BuildConfig;
import android.util.Log;

import com.tf.credrandroidassignment.model.SearchResult;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by kamran on 28/10/17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class SearchHelperTest {

    private static final String TAG = SearchHelperTest.class.getSimpleName();

    private Context mContext;
    private SearchHelper mSearchHelper;

    @Before
    public void setup() {
        ShadowLog.stream = System.out;

        mContext = RuntimeEnvironment.application.getApplicationContext();
        mSearchHelper = new SearchHelper(mContext);
    }

    @Test
    public void fetchSearchPage_shouldReceiveAPage() throws JSONException, Network.NoInternetException, IOException {
        assertEquals(200, mSearchHelper.fetchSearchPage("test").getResponseCode());
    }

    @Test
    public void searchForAnything_shouldReturnNonNullValue() throws JSONException, Network.NoInternetException, IOException, InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);

        mSearchHelper.fetchSearchResults("anything", new SearchHelper.SearchResultsFetchedListener() {
            @Override
            public void onFetched(List<SearchResult> results) {
                assertNotNull(results);

                latch.countDown();
            }
        });

        latch.await(5, TimeUnit.MINUTES);
    }

    @Test
    public void searchForApple_shouldReturnAppleProducts() throws JSONException, Network.NoInternetException, IOException, InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);

        final String query = "Samsung";

        mSearchHelper.fetchSearchResults(query, new SearchHelper.SearchResultsFetchedListener() {
            @Override
            public void onFetched(List<SearchResult> results) {
                Log.i(TAG, "First result " + results.get(0).getItemName());

                assertTrue(results.get(0).getItemName().contains(query));

                latch.countDown();
            }
        });

        latch.await(5, TimeUnit.MINUTES);
    }
}
