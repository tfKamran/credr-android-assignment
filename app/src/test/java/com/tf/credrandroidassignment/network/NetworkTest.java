package com.tf.credrandroidassignment.network;

import android.content.ContentValues;
import android.content.Context;
import android.support.v4.BuildConfig;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by kamran on 28/10/17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class NetworkTest {

    private Context context;

    @Before
    public void setup() {
        context = RuntimeEnvironment.application.getApplicationContext();
    }

    @Test
    public void getResponseCode204_receiveResponseCode204() throws JSONException, Network.NoInternetException, IOException {
        assertEquals(Network.requestGet(context, "http://connectivitycheck.android.com/generate_204", new ContentValues())
                        .getResponseCode(),
                204);
    }
}
