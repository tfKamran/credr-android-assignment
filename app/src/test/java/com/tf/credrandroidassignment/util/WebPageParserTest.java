package com.tf.credrandroidassignment.util;

import android.content.Context;
import android.support.v4.BuildConfig;
import android.util.Log;

import com.tf.credrandroidassignment.model.SearchResult;
import com.tf.credrandroidassignment.network.Network;
import com.tf.credrandroidassignment.network.SearchHelper;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import static org.junit.Assert.assertNotEquals;

/**
 * Created by kamran on 28/10/17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class WebPageParserTest {

    private static final String TAG = WebPageParserTest.class.getSimpleName();

    private Context mContext;
    private WebPageParser mWebPageParser;

    @Before
    public void setup() {
        ShadowLog.stream = System.out;

        mContext = RuntimeEnvironment.application.getApplicationContext();
        mWebPageParser = new WebPageParser();
    }

    @Test
    public void feedSamplePage_shouldBeAbleToParseResults() throws JSONException,
            Network.NoInternetException, IOException, InterruptedException, ParserConfigurationException,
            SAXException, XPathExpressionException {
        final Network.NetworkResponse networkResponse = getSamplePage();

        final List<SearchResult> parsedResults = mWebPageParser.parse(networkResponse.getResponseString());

        Log.i(TAG, "Found " + String.valueOf(parsedResults.size()) + " items");
        for (SearchResult parsedResult : parsedResults) {
            Log.i(String.valueOf(parsedResults.indexOf(parsedResult)), parsedResult.getThumbnailUrl() + " - " + parsedResult.getItemName());
        }

        assertNotEquals(0, parsedResults.size());
    }

    private Network.NetworkResponse getSamplePage() throws Network.NoInternetException, IOException, JSONException {
        return new SearchHelper(mContext).fetchSearchPage("mobile");
    }
}
