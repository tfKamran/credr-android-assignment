# CredR Android Assignment

## Problem Statement

Make an App with a search bar and list view so that when user types anything in the search bar. We should get the search results from Snapdeal webpage with that particular search word and get the images of these search results and show it up in the search list. (Hint: Use x-path)

Data source:  Use some kind of webservice or https://www.snapdeal.com/search?keyword=mobile&sort=rlvncy and show data on listview.

Please note above url will give you an HTML and not JSON/XML. Therefore, use a library to parse the HTML using x-path to get the details.

The response won't be in JSON format, it will be an HTML which should be parsed (hint using x-path) to get the list. 
